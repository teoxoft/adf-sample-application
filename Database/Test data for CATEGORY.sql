REM INSERTING into CATEGORY
Insert into CATEGORY (ID,NAME,DESCRIPTION) values (1,'Sport Clothes','This category contains all products related with sport clothes');
Insert into CATEGORY (ID,NAME,DESCRIPTION) values (2,'IT books','All books related with IT world');
Insert into CATEGORY (ID,NAME,DESCRIPTION) values (3,'Sport Equipment','Equipment for different kinds of sport');
Insert into CATEGORY (ID,NAME,DESCRIPTION) values (4,'Electronic Devices','Everything that is related with Hi-Tech devices');
Insert into CATEGORY (ID,NAME,DESCRIPTION) values (5,'Home and Garden','Everything for your home and garden');
Insert into CATEGORY (ID,NAME,DESCRIPTION) values (8,'Food','All food goods');
