REM INSERTING into PRODUCT
Insert into PRODUCT (ID,NAME,DESCRIPTION,PRICE,CATEGORY_ID) values (1,'Adidas ball 5','Brazuca',100,3);
Insert into PRODUCT (ID,NAME,DESCRIPTION,PRICE,CATEGORY_ID) values (2,'Soccer cleats (Nike)','Improve your game significantly with these cleats!',75,1);
Insert into PRODUCT (ID,NAME,DESCRIPTION,PRICE,CATEGORY_ID) values (25,'Usstc Pipe Glove red by VOLCOM','Features: 
fingered gloves, velcro fastener, firm-grip fingers, firm-grip palms
zipper
comfortable material, water-resistant material, breathable material
logo patch
Waterproof and Breathable Insert
V-Science Faux Leather and Stretch Nylon
Silicone Printed Palm
Brushed Tricot Fixed Lining
Stretch Slip-On
Velcro Wrist Closure
Suede Nose Wipe Panel
Ergonomically Curved Fingers',99.23,1);
Insert into PRODUCT (ID,NAME,DESCRIPTION,PRICE,CATEGORY_ID) values (23,'Morris Snow Jacket black','Material: 
 Lining: 50% polyester, 50% nylon or polyamide
 Outer Shell: 100% nylon or polyamide
 Padding: 100% polyester',150.03,1);
Insert into PRODUCT (ID,NAME,DESCRIPTION,PRICE,CATEGORY_ID) values (24,'VOLCOM Oxnard Jacket charcoal','Features: 
regular fit
material: V-Science 2-Layer Shell
water column 15.000 mm
breathability 10.000 g/m2
all seams taped
hood with high collar
adjustable hood with visor
rubberized watertight YKK® Zipper, zip with logo puller and whistle, two-way zip
chin guard on the collar
forearm ventilation with mesh inset
lycra cuffs',500.27,1);
Insert into PRODUCT (ID,NAME,DESCRIPTION,PRICE,CATEGORY_ID) values (26,'Usstc Pipe Glove white by VOLCOM','Material: 50% nylon or polyamide, 40% polyurethane, 10% polyester',76.43,1);
Insert into PRODUCT (ID,NAME,DESCRIPTION,PRICE,CATEGORY_ID) values (27,'Code Complete (Steve Macconnell)','Widely considered one of the best practical guides to programming, Steve McConnell’s original CODE COMPLETE has been helping developers write better software for more than a decade. Now this classic book has been fully updated and revised with leading-edge practices—and hundreds of new code samples—illustrating the art and science of software construction. Capturing the body of knowledge available from research, academia, and everyday',35,2);
Insert into PRODUCT (ID,NAME,DESCRIPTION,PRICE,CATEGORY_ID) values (28,'CLR via C# (4th Edition) (Developer Reference)','Dig deep and master the intricacies of the common language runtime, C#, and .NET development. Led by programming expert Jeffrey Richter, a longtime consultant to the Microsoft .NET team - you’ll gain pragmatic insights for building robust, reliable, and responsive apps and components.',40.01,2);
Insert into PRODUCT (ID,NAME,DESCRIPTION,PRICE,CATEGORY_ID) values (29,'adidas Brazuca 2014 Top Glider Soccer Ball','Improve your skills, push yourself harder, and earn respect for your sport with the Brazuca 2014 Top Glider soccer ball from adidas®. Inspired by the design of the 2014 FIFA World Cup™ Official Match Ball, this training ball is machine-stitched with a nylon-wound carcass/TPU cover for touch and durability. Butyl bladder construction supports air and shape retention so your time is spent on practice, not pumping up the soccer ball.',35.4,3);
Insert into PRODUCT (ID,NAME,DESCRIPTION,PRICE,CATEGORY_ID) values (30,'adidas Estadio Team Backpack II','Transport your gear to and from the field in style with the adidas® Estadio Team Backpack II. Built with soccer in mind, this backpack is constructed with multiple compartments to conveniently organize and store all of your gear. LoadSpring™ technology adds comfort to the shoulder straps so you can carry more equipment. FreshPAK™ shoe compartment keeps gear fresh throughout the season.',55,3);
Insert into PRODUCT (ID,NAME,DESCRIPTION,PRICE,CATEGORY_ID) values (31,'HEAD Ti.S1 Pro Tennis Racquet','The HEAD® Ti.S1 Pro Tennis Racquet is the ideal lightweight racquet designed to bring out your best on the tennis court',87.54,3);
Insert into PRODUCT (ID,NAME,DESCRIPTION,PRICE,CATEGORY_ID) values (32,'SOLE F80 Treadmill','Bring the outdoors into your home fitness area with the high performance SOLE F80 Treadmill. ',1503.43,3);
Insert into PRODUCT (ID,NAME,DESCRIPTION,PRICE,CATEGORY_ID) values (33,'Lenovo p770','The Lenovo Lephone P770 Dual-core is an android 4.1 smartphone with built in  3G, Dual sim capabilities with a high resolution 4.5 inch Capacitive multitouch',250.43,4);
Insert into PRODUCT (ID,NAME,DESCRIPTION,PRICE,CATEGORY_ID) values (34,'Nokia 3310','mobile phone that can also be used as hammer',15,4);
Insert into PRODUCT (ID,NAME,DESCRIPTION,PRICE,CATEGORY_ID) values (35,'WIRELESS DOOR/WINDOW CONTACT AND MAGNET 5816WMWH','COMPATIBLE WITH LYNX OR VISTA PANELS',23.43,5);
Insert into PRODUCT (ID,NAME,DESCRIPTION,PRICE,CATEGORY_ID) values (36,'NEW ADEMCO HONEYWELL 5804 WIRELESS TRANSMITTER FOB REMOTE HOME SECURITY SYS','Up for auction is a NEW Ademco 4 button wireless key transmitter. It says Honeywell on the back and also N219. The batteries may need to be changed. This was bought by my mother and has never been use.

Any questions feel free to email me.',10.32,5);
Insert into PRODUCT (ID,NAME,DESCRIPTION,PRICE,CATEGORY_ID) values (37,'Electronic Digital Lock Keypad Safe Box','Best Choice Products is proud to present this brands new electronic digital safe. This safe is a “must have” for security conscious business managers. It’s perfect storing money, jewelry, hand guns, and any other valuables. It’s great for convenient stores, restaurants, bars, hotels/motels, small cash businesses, pawn shops, etc. We purchase our products directly from the manufacturer, so you know you‘re getting the best prices possible.',40.3,5);
Insert into PRODUCT (ID,NAME,DESCRIPTION,PRICE,CATEGORY_ID) values (38,'4.5W Solar Panel - 6V / 720mA - 165 x 165 mm - Epoxy Coated - Monocrystalline','This is a super efficient monocrystalline solar panel, coated in epoxy resin for protection. Monocrystalline panels have higher efficiency that polycrystalline.',14.65,5);
Insert into PRODUCT (ID,NAME,DESCRIPTION,PRICE,CATEGORY_ID) values (40,'Bread','Bread is a staple food prepared from a dough of flour and water, usually by  baking.',0.95,8);
