package com.teoxoft.adfsa.view.controllers;


import com.teoxoft.adfsa.model.entities.Category;
import com.teoxoft.adfsa.model.entities.Product;
import com.teoxoft.adfsa.model.facades.CategoryFacadeLocal;
import com.teoxoft.adfsa.model.facades.ProductFacadeLocal;
import com.teoxoft.adfsa.model.filters.ProductFilter;
import com.teoxoft.adfsa.model.filters.SearchPattern;
import com.teoxoft.adfsa.view.utils.ApplicationUtils;
import com.teoxoft.adfsa.view.utils.ExceptionUtils;
import com.teoxoft.adfsa.view.utils.LoggerHelper;
import com.teoxoft.adfsa.view.utils.MessagesUtils;

import com.teoxoft.adfsa.view.utils.UIElementsUtils;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import javax.ejb.EJB;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import oracle.adf.share.logging.ADFLogger;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.event.DialogEvent;

import org.apache.myfaces.trinidad.model.SortCriterion;


/**
 * @author: Gennadiy Pervukhin
 * Created: 16-10-2014
 */
@ManagedBean
@ViewScoped
public class ManageProductsController
{
    //
    //Constructors
    //
    public ManageProductsController()
    {
        super();
    }
    //
    
    //
    //Constants
    //
    private static final SortCriterion DEFAULT_SORT_CRITERION =
        new SortCriterion("name", true);
    //
    
    //
    //Variables
    //
    @EJB
    private CategoryFacadeLocal categoryFacade;
    @EJB
    private ProductFacadeLocal productFacade;
    
    //filter for the table
    private ProductFilter productsFilter;
    
    //data from DB
    private List<Category> allCategories;
    private List<Product> products;
    
    //new product
    private Product newProduct;
    
    //UI-elements
    private RichTable productsTable;
    private RichPopup createProductPopup;
    //
    
    
    //
    //Properties
    //
    public ProductFilter getProductsFilter() {
        return productsFilter;
    }

    public List<Category> getAllCategories() {
        return allCategories;
    }

    public List<Product> getProducts() {
        return products;
    }
    
    public Product getNewProduct() {
        return newProduct;
    }
    
    public void setCreateProductPopup(RichPopup createProductPopup) {
        this.createProductPopup = createProductPopup;
    }
    public RichPopup getCreateProductPopup() {
        return createProductPopup;
    }

    public void setProductsTable(RichTable table)
    {
        //if this property is called the 1st time then we should configure the UI-table
        if (this.productsTable == null)
        {
            configureUITable(table);
        }
        this.productsTable = table;
    }
    public RichTable getProductsTable() {
        return productsTable;
    }
    //
    
    
    //
    //Private methods
    //
    @PostConstruct
    private void initialize()
    {
        //1) initialize the filter for products
        this.initializeProductsFilter();
        
        //2) get all categories
        this.allCategories = this.categoryFacade.getAllCategories();
        
        //3) refresh the list of products
        this.refreshProductsList();
    }
    
    private void initializeProductsFilter()
    {
        //create default empty filter
        this.productsFilter = new ProductFilter();
        this.productsFilter.setPatternForName(new SearchPattern(null, false, true));
    }
    
    private void refreshProductsList()
    {
        this.products = this.productFacade.getProductsByFilter(this.productsFilter);
    }
    
    private static void configureUITable(RichTable table)
    {
        if (table != null)
        {
            List<SortCriterion> criterions = new ArrayList<SortCriterion>();
            criterions.add(DEFAULT_SORT_CRITERION);
            table.setSortCriteria(criterions);
        }
    }
    
    private Product getSelectedProduct()
    {
        int indexOfSelectedRow = UIElementsUtils.getIndexOfSelectedTableRow(this.productsTable);
        if (indexOfSelectedRow != -1)
        {
            return this.products.get(indexOfSelectedRow);
        }
        return null;
    }
    //
    
    
    //
    //Public methods
    //
    public void onFilterButtonClicked()
    {
        //refresh the list of products using the current filter
        this.refreshProductsList();
    }
    
    public void onResetButtonClicked()
    {
        //1) re-initialize the filter
        this.initializeProductsFilter();
        //2) refresh the list of products using the default filter
        this.refreshProductsList();
    }
    
    public void onNewProductDialogButtonClicked()
    {
        //1) initialize an empty object for a new product
        this.newProduct = new Product();
        //2) open the popup
        this.createProductPopup.show(new RichPopup.PopupHints());
    }
    
    public void onCreateProductButtonClicked(DialogEvent dialogEvent)
    {
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.ok) && this.newProduct != null)
        {
            try
            {
                //1) try to insert a new product
                this.productFacade.persistProduct(this.newProduct);
                //2) refresh the list of products
                this.refreshProductsList();
            }
            catch (Exception error)
            {
                MessagesUtils.showMessageOnOperationFailed(error);
            }
        }
    }
    
    public void onDeleteProductButtonClicked(DialogEvent dialogEvent)
    {
        //1) try to get a selected product
        Product selectedProduct = this.getSelectedProduct();
        if (selectedProduct == null)
        {
            MessagesUtils.createMessage(LoggerHelper.LOGGER.getFormattedMessage(
                "messages.error.noRecordSelected"), FacesMessage.SEVERITY_WARN);
            return;
        }
        
        //2) process deletion
        try
        {
            //2.1) try to delete the selected product
            this.productFacade.removeProduct(selectedProduct);
            //2.2) delete the product from the list
            this.products.remove(selectedProduct);
        }
        catch (Exception error)
        {
            MessagesUtils.showMessageOnOperationFailed(error);
        }
    }
    //
}