package com.teoxoft.adfsa.view.controllers;

import com.teoxoft.adfsa.view.utils.ApplicationUtils;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.commons.lang3.StringUtils;

@ManagedBean
@ViewScoped
public class TemplateBean
{
    //
    //Constructors
    //
    public TemplateBean()
    {
        super();
    }
    //
    
    //
    //Variables
    //
    private String currentURL;
    //
    
    //
    //Properties
    //
    public String getCurrentURL()
    {
        //if the variable is empty then we get and cache it, else we return a cached value
        if (StringUtils.isBlank(currentURL))
        {
            currentURL = ApplicationUtils.getCurrentURL(true);
        }
        return this.currentURL;
    }
    //
}
