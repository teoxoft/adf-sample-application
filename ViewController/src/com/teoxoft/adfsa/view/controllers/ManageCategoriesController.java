package com.teoxoft.adfsa.view.controllers;


import com.teoxoft.adfsa.model.entities.Category;
import com.teoxoft.adfsa.model.facades.CategoryFacadeLocal;
import com.teoxoft.adfsa.view.utils.LoggerHelper;
import com.teoxoft.adfsa.view.utils.MessagesUtils;
import com.teoxoft.adfsa.view.utils.UIElementsUtils;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import javax.ejb.EJB;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.event.DialogEvent;

import org.apache.myfaces.trinidad.model.SortCriterion;


/**
 * @author: Gennadiy Pervukhin
 * Created: 17-10-2014
 */
@ManagedBean
@ViewScoped
public class ManageCategoriesController
{
    //
    //Constructors
    //
    public ManageCategoriesController()
    {
        super();
    }
    //
    
    //
    //Constants
    //
    private static final SortCriterion DEFAULT_SORT_CRITERION =
        new SortCriterion("name", true);
    //
    
    //
    //Variables
    //
    @EJB
    private CategoryFacadeLocal categoryFacade;
    
    //data from DB
    private List<Category> categories;
    
    //new category
    private Category newCategory;
    
    //UI-elements
    private RichTable categoriesTable;
    private RichPopup createCategoryPopup;
    //
    
    
    //
    //Properties
    //
    public List<Category> getCategories() {
        return categories;
    }
    
    public Category getNewCategory() {
        return newCategory;
    }
    
    public void setCreateCategoryPopup(RichPopup createCategoryPopup) {
        this.createCategoryPopup = createCategoryPopup;
    }
    public RichPopup getCreateCategoryPopup() {
        return createCategoryPopup;
    }

    public void setCategoriesTable(RichTable table)
    {
        //if this property is called the 1st time then we should configure the UI-table
        if (this.categoriesTable == null)
        {
            configureUITable(table);
        }
        this.categoriesTable = table;
    }
    public RichTable getCategoriesTable() {
        return categoriesTable;
    }
    //
    
    
    //
    //Private methods
    //
    @PostConstruct
    private void initialize()
    {
        //refresh the list of categories
        this.refreshCategoriesList();
    }
    
    private void refreshCategoriesList()
    {
        this.categories = this.categoryFacade.getAllCategories();
    }
    
    private static void configureUITable(RichTable table)
    {
        if (table != null)
        {
            List<SortCriterion> criterions = new ArrayList<SortCriterion>();
            criterions.add(DEFAULT_SORT_CRITERION);
            table.setSortCriteria(criterions);
        }
    }
    
    private Category getSelectedCategory()
    {
        int indexOfSelectedRow = UIElementsUtils.getIndexOfSelectedTableRow(this.categoriesTable);
        if (indexOfSelectedRow != -1)
        {
            return this.categories.get(indexOfSelectedRow);
        }
        return null;
    }
    //
    
    
    //
    //Public methods
    //
    public void onNewCategoryDialogButtonClicked()
    {
        //1) initialize an empty object for a new category
        this.newCategory = new Category();
        //2) open the popup
        this.createCategoryPopup.show(new RichPopup.PopupHints());
    }
    
    public void onCreateCategoryButtonClicked(DialogEvent dialogEvent)
    {
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.ok) && this.newCategory != null)
        {
            try
            {
                //1) try to insert a new category
                this.categoryFacade.persistCategory(this.newCategory);
                //2) refresh the list of categories
                this.refreshCategoriesList();
            }
            catch (Exception error)
            {
                MessagesUtils.showMessageOnOperationFailed(error);
            }
        }
    }
    
    public void onDeleteCategoryButtonClicked(DialogEvent dialogEvent)
    {
        //1) try to get a selected category
        Category selectedCategory = this.getSelectedCategory();
        if (selectedCategory == null)
        {
            MessagesUtils.createMessage(LoggerHelper.LOGGER.getFormattedMessage(
                "messages.error.noRecordSelected"), FacesMessage.SEVERITY_WARN);
            return;
        }
        
        //2) process deletion
        try
        {
            //2.1) try to delete the selected category
            this.categoryFacade.removeCategory(selectedCategory);
            //2.2) delete the category from the list
            this.categories.remove(selectedCategory);
        }
        catch (Exception error)
        {
            MessagesUtils.showMessageOnOperationFailed(error);
        }
    }
    //
}
