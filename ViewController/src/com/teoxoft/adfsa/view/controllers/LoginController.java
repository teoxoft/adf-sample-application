package com.teoxoft.adfsa.view.controllers;


import com.teoxoft.adfsa.view.utils.ApplicationUtils;
import com.teoxoft.adfsa.view.utils.LoggerHelper;
import com.teoxoft.adfsa.view.utils.MessagesUtils;

import java.io.IOException;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

import weblogic.security.URLCallbackHandler;
import weblogic.security.services.Authentication;

import weblogic.servlet.security.ServletAuthentication;


/**
 * @author: Gennadiy Pervukhin
 * Created: 16-10-2014
 */
@ManagedBean
@RequestScoped
public class LoginController
{
    //
    //Constructors
    //
    public LoginController()
    {
        super();
    }
    //
    
    //
    //Constants
    //
    private static final String HOME_PAGE_RELATIVE_URL = "home";
    //
    
    //
    //Variables
    //
    private String username = "admin";
    private String password = "12345678";
    //
    
    //
    //Properties
    //
    public void setUsername(String username) {
        this.username = username;
    }
    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String getPassword() {
        return password;
    }
    //
    
    //
    //Private methods
    //
    private void handleFailureOfAuthentication(String errorMessage)
    {
        //1) show error message
        if (StringUtils.isNotBlank(errorMessage))
        {
            MessagesUtils.createMessage(errorMessage, FacesMessage.SEVERITY_ERROR);
        }
        //2) reset the password
        this.password = null;
    }
    //
    
    //
    //Public methods
    //
    public String onLogout()
    {
        FacesContext fctx = FacesContext.getCurrentInstance();
        ExternalContext ectx = fctx.getExternalContext();
        String url = ectx.getRequestContextPath() + "/adfAuthentication?logout=true&end_url=/faces/home";
        try
        {
            ectx.redirect(url);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        fctx.responseComplete();
        return null;
    }

    public void onLogin() throws LoginException
    {
        //extract the current context and request
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)ctx.getExternalContext().getRequest();
        try
        {
            //initialize callback handler
            byte[] passwordBytes = this.password.getBytes();
            CallbackHandler handler = new URLCallbackHandler(this.username, passwordBytes);
            
            //do login action
            Subject loginSubject = Authentication.login(handler);
            
            //work with servlet authentication
            ServletAuthentication.runAs(loginSubject, request);
            ServletAuthentication.generateNewSessionID(request);
            
            //set redirect after a successful result
            ApplicationUtils.redirectToURL(HOME_PAGE_RELATIVE_URL);
        }
        catch (FailedLoginException flError)
        {
            String errorMessage = LoggerHelper.LOGGER.getFormattedMessage(
                "messages.error.invalidUserCredentails");
            this.handleFailureOfAuthentication(errorMessage);
        }
        catch (Exception error)
        {
            String errorMessage = LoggerHelper.LOGGER.getFormattedMessage(
                "messages.error.authenticationFailed");
            this.handleFailureOfAuthentication(errorMessage);
        }
    }
    //
}
