package com.teoxoft.adfsa.view.utils;

import java.io.IOException;

import javax.faces.application.ViewHandler;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import javax.servlet.http.HttpServletRequest;


public class ApplicationUtils
{
    //
    //Private static methods
    //
    private static String extractURLPrefixFromRequest(HttpServletRequest request)
    {
        StringBuilder builder = new StringBuilder(32);
        builder.append(request.getScheme());
        builder.append("://");
        builder.append(request.getServerName());
        builder.append(':');
        builder.append(request.getServerPort());

        return builder.toString();
    }
    //
    
    //
    //Public static methods
    //
    public static void redirectToURL(String forwardUrl)
    {
        FacesContext ctx = FacesContext.getCurrentInstance();
        try
        {
            ctx.getExternalContext().redirect(forwardUrl);
        }
        catch (IOException ioError)
        {
            ioError.printStackTrace();
        }
        ctx.responseComplete();
    }
    
    public static String getCurrentURL(boolean trancateParameters)
    {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext eContext = context.getExternalContext();

        //STEP #1: extract site name
        String siteAddress;
        Object requestObject = eContext.getRequest();
        if (requestObject instanceof HttpServletRequest)
        {
            siteAddress = extractURLPrefixFromRequest((HttpServletRequest)requestObject);
        }
        else
        {
            siteAddress = "";
        }

        //STEP #2: extract page name and GET-parameters
        ViewHandler handler = context.getApplication().getViewHandler();
        String pageAddress = handler.getActionURL(context, context.getViewRoot().getViewId());

        //STEP #3: trancate GET-parameters if it's needed
        if (trancateParameters)
        {
            int startOfParameters = pageAddress.indexOf("?");
            if (startOfParameters != -1)
            {
                pageAddress = pageAddress.substring(0, startOfParameters);
            }
        }

        String fullURL = (siteAddress + pageAddress);
        return fullURL;
    }
    //
}