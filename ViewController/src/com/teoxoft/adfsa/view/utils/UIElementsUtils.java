package com.teoxoft.adfsa.view.utils;

import java.util.Iterator;

import oracle.adf.view.rich.component.rich.data.RichTable;

import org.apache.myfaces.trinidad.model.RowKeySet;

/**
 * @author: Gennadiy Pervukhin
 * Created: 17-10-2014
 */
public class UIElementsUtils
{
    //
    //Public static methods which work with UI-table
    //
    public static Integer getIndexOfSelectedTableRow(RichTable table)
    {
        Integer indexOfSelectedRow = -1;
        
        if (table != null)
        {
            RowKeySet selectedRowKeySet = table.getSelectedRowKeys();
            if (selectedRowKeySet != null && selectedRowKeySet.size() > 0)
            {
                Iterator iterator = selectedRowKeySet.iterator();
                indexOfSelectedRow = (Integer)iterator.next();
            }
        }
        return indexOfSelectedRow;
    }
    //
}