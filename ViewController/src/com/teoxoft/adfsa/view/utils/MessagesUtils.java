package com.teoxoft.adfsa.view.utils;


import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;


public class MessagesUtils
{
    //
    //Public static methods
    //
    public static void createMessage(String message, FacesMessage.Severity severity)
    {
        createMessage(null, message, severity);
    }
    
    public static void createMessage(UIComponent component,  String message, FacesMessage.Severity severity)
    {
        FacesContext ctx = FacesContext.getCurrentInstance();
        FacesMessage msg = new FacesMessage(severity, null, message);
        ctx.addMessage((component == null ? null : component.getClientId()), msg);
    }
    
    public static boolean hasMessages()
    {
        return !FacesContext.getCurrentInstance().getMessageList().isEmpty();    
    }
    
    public static void showMessageOnOperationFailed(Exception error)
    {
        //cause of the error
        Throwable lowestCause = ExceptionUtils.extractCause(error, true);
        
        //show error message
        String errorMessage = LoggerHelper.LOGGER.getFormattedMessage(
            "messages.error.operationFailedByReason",
            new String[] {lowestCause.getMessage()});
        MessagesUtils.createMessage(errorMessage, FacesMessage.SEVERITY_ERROR);
    }
    //
}