package com.teoxoft.adfsa.view.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * @author: Gennadiy Pervikhin
 * Created: 17-10-2014
 */
public class ExceptionUtils
{
    //
    //Public static methods
    //
    public static Throwable extractCause(Exception error, boolean analyzeLowestCause)
    {
        //we consider that the passed exception can be the lowest cause
        Throwable lowestCause = error;
        //analyze lower causes
        if (analyzeLowestCause)
        {
            //while we have a more detailed cause with non empty message which differs
            //from previous extracted cause we extract further causes
            while (lowestCause.getCause() != null && lowestCause.getCause() != lowestCause
                && StringUtils.isNotBlank(lowestCause.getCause().getMessage()))
            {
                lowestCause = lowestCause.getCause();
            }
        }

        return lowestCause;
    }
    //
}