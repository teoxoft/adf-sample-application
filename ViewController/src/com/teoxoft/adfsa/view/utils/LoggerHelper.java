package com.teoxoft.adfsa.view.utils;

import oracle.adf.share.logging.ADFLogger;

/**
 * @author: Gennadiy Pervukhin
 * Created: 16-10-2014
 */
public class LoggerHelper
{
    public static ADFLogger LOGGER = ADFLogger.createADFLogger(
        Package.getPackage("com.teoxoft.adfsa.view.controllers"),
        "com.teoxoft.adfsa.view.resources.SampleAppBundle");
}