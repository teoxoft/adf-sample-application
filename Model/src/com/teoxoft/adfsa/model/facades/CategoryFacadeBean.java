package com.teoxoft.adfsa.model.facades;

import com.teoxoft.adfsa.model.entities.Category;

import java.util.List;

import javax.annotation.Resource;

import javax.ejb.SessionContext;
import javax.ejb.Stateless;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless(name = "CategoryFacade", mappedName = "SampleApplication-Model-CategoryFacade")
public class CategoryFacadeBean implements CategoryFacadeLocal
{
    //
    //Constructors
    //
    public CategoryFacadeBean() {
    }
    //
    
    //
    //Variables
    //
    @Resource
    SessionContext sessionContext;
    @PersistenceContext(unitName = "SampleAdfApplication")
    private EntityManager entityManager;
    //
    
    //
    //Public methods
    //
    public Category persistCategory(Category category) {
        entityManager.persist(category);
        return category;
    }

    public Category mergeCategory(Category category) {
        return entityManager.merge(category);
    }

    public void removeCategory(Category category) {
        category = entityManager.find(Category.class, category.getId());
        entityManager.remove(category);
    }

    /** <code>select o from Category o</code> */
    public List<Category> getAllCategories() {
        return entityManager.createNamedQuery("Category.findAll").getResultList();
    }
    //
}
