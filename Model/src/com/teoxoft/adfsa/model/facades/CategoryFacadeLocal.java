package com.teoxoft.adfsa.model.facades;

import com.teoxoft.adfsa.model.entities.Category;

import java.util.List;

import javax.ejb.Local;

@Local
public interface CategoryFacadeLocal {
    Category persistCategory(Category category);

    Category mergeCategory(Category category);

    void removeCategory(Category category);

    List<Category> getAllCategories();
}
