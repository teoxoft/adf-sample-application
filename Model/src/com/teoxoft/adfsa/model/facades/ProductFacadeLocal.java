package com.teoxoft.adfsa.model.facades;


import com.teoxoft.adfsa.model.entities.Product;
import com.teoxoft.adfsa.model.filters.ProductFilter;

import java.util.List;

import javax.ejb.Local;


@Local
public interface ProductFacadeLocal
{
    Product persistProduct(Product product);

    Product mergeProduct(Product product);

    void removeProduct(Product product);

    List<Product> getAllProducts();
    
    List<Product> getProductsByFilter(ProductFilter filter);
}