package com.teoxoft.adfsa.model.facades;

import com.teoxoft.adfsa.model.entities.Product;

import com.teoxoft.adfsa.model.filters.ProductFilter;
import com.teoxoft.adfsa.model.utils.sql.LogicOperator;
import com.teoxoft.adfsa.model.utils.sql.SelectQueryBuilder;

import com.teoxoft.adfsa.model.utils.sql.SqlUtils;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import javax.ejb.SessionContext;
import javax.ejb.Stateless;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;

@Stateless(name = "ProductFacade", mappedName = "SampleApplication-Model-ProductFacade")
public class ProductFacadeBean implements ProductFacadeLocal
{
    //
    //Constructors
    //
    public ProductFacadeBean() {
    }
    //
    
    //
    //Variables
    //
    @Resource
    SessionContext sessionContext;
    @PersistenceContext(unitName = "SampleAdfApplication")
    private EntityManager entityManager;
    //
    
    //
    //Private methods
    //
    private void setCondition(SelectQueryBuilder queryBuilder, ProductFilter filter)
    {
        List<String> listOfConditions = new ArrayList<String>();
        
        //- add a condition by category Id
        if (filter.getCategoryId() != null)
        {
            listOfConditions.add("o.categoryId = :categoryId");
            queryBuilder.getParameters().put("categoryId", filter.getCategoryId());
        }
        
        //- add a condition by a search pattern for name
        SqlUtils.addCondition(filter.getPatternForName(), "o.name",
            "namePattern", listOfConditions, queryBuilder);

        //- build a final condition by joining conditions via AND-operator
        queryBuilder.setCondition(
            SqlUtils.joinConditions(listOfConditions, LogicOperator.AND));
    }
    //
    
    //
    //Public methods
    //
    public Product persistProduct(Product product) {
        entityManager.persist(product);
        return product;
    }

    public Product mergeProduct(Product product) {
        return entityManager.merge(product);
    }

    public void removeProduct(Product product) {
        product = entityManager.find(Product.class, product.getId());
        entityManager.remove(product);
    }

    /** <code>select o from Product o</code> */
    public List<Product> getAllProducts() {
        return entityManager.createNamedQuery("Product.findAll").getResultList();
    }
    
    public List<Product> getProductsByFilter(ProductFilter filter)
    {
        if (filter == null)
        {
            return null;
        }
        
        //STEP #1: initialize and fill the query builder
        SelectQueryBuilder queryBuilder = new SelectQueryBuilder("Product", "o");
        //- set condition
        this.setCondition(queryBuilder, filter);
        
        //STEP #2: build the query
        Query query = queryBuilder.buildQuery(this.entityManager);
        
        //STEP #3: return the result list
        return query.getResultList();
    }
    //
}
