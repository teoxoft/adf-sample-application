package com.teoxoft.adfsa.model.entities;

import java.io.Serializable;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@NamedQueries( { @NamedQuery(name = "Product.findAll", query = "select o from Product o") })
@Table(name = "PRODUCT", schema = "SAMPLE_ADF_USER")
public class Product implements Serializable
{
    //
    //Constructors
    //
    public Product() {
    }
    //
    
    //
    //Variables
    //
    @Id
    @Column(nullable = false)
    @GeneratedValue(generator = "ProductIdSequence", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "ProductIdSequence", schema = "SAMPLE_ADF_USER", sequenceName = "PRODUCT_ID_SEQ", allocationSize = 1)
    private BigDecimal id;
    
    @Column(name = "CATEGORY_ID", nullable = false)
    private BigDecimal categoryId;
    
    @Column(nullable = false, length = 100)
    private String name;
    @Column(length = 4000)
    private String description;
    @Column(nullable = false)
    private BigDecimal price;
    //
    
    //
    //Relations
    //
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CATEGORY_ID", insertable = false, updatable = false)
    private Category category;
    //
    
    //
    //Public properties
    //
    public BigDecimal getId() {
        return id;
    }
    public void setId(BigDecimal id) {
        this.id = id;
    }
    
    public BigDecimal getCategoryId() {
        return categoryId;
    }
    public void setCategoryId(BigDecimal categoryId) {
        this.categoryId = categoryId;
    }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
    public Category getCategory() {
        return category;
    }
    //
}