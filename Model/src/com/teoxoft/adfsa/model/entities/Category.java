package com.teoxoft.adfsa.model.entities;

import java.io.Serializable;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@NamedQueries( { @NamedQuery(name = "Category.findAll", query = "select o from Category o order by o.name") })
@Table(name = "CATEGORY", schema = "SAMPLE_ADF_USER")
public class Category implements Serializable
{
    //
    //Constructors
    //
    public Category() {
    }
    //
    
    //
    //Variables
    //
    @Id
    @Column(nullable = false)
    @GeneratedValue(generator = "CategoryIdSequence", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "CategoryIdSequence", schema = "SAMPLE_ADF_USER", sequenceName = "CATEGORY_ID_SEQ", allocationSize = 1)
    private BigDecimal id;
    
    @Column(nullable = false, unique = true, length = 100)
    private String name;
    @Column(length = 4000)
    private String description;
    //
    
    //
    //Public properties
    //
    public BigDecimal getId() {
        return id;
    }
    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    //
}
