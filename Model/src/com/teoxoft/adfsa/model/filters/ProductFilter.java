package com.teoxoft.adfsa.model.filters;

import java.math.BigDecimal;

/**
 * @author: Gennadiy Pervukhin
 * Created: 16-10-2014
 */
public class ProductFilter
{
    //
    //Constructors
    //
    public ProductFilter()
    {
        super();
    }
    //
    
    //
    //Variables
    //
    private BigDecimal categoryId;
    private SearchPattern patternForName;
    //
    
    //
    //Properties
    //
    public void setCategoryId(BigDecimal categoryId) {
        this.categoryId = categoryId;
    }
    public BigDecimal getCategoryId() {
        return categoryId;
    }

    public void setPatternForName(SearchPattern patternForName) {
        this.patternForName = patternForName;
    }
    public SearchPattern getPatternForName() {
        return patternForName;
    }
    //
}