package com.teoxoft.adfsa.model.filters;

import org.apache.commons.lang3.StringUtils;

/**
 * @author: Gennadiy Pervukhin
 * Created: 16-10-2014
 */
public class SearchPattern
{
    public SearchPattern(String searchValue, boolean isCaseSensitive,
        boolean canContainSpecialChars)
    {
        this.setPattern(searchValue);
        this.isCaseSensitive = isCaseSensitive;
        this.canContainSpecialChars = canContainSpecialChars;
    }
    
    //
    //Variables
    //
    private String pattern;
    private boolean isCaseSensitive;
    private boolean canContainSpecialChars;
    //
    
    //
    //Properties
    //
    public String getPattern() {
        return pattern;
    }
    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public boolean isCaseSensitive() {
        return isCaseSensitive;
    }
    
    public boolean canContainSpecialChars() {
        return canContainSpecialChars;
    }
    //
    
    //
    //Public methods
    //
    public boolean isNotBlank()
    {
        return StringUtils.isNotBlank(this.pattern);
    }
    //
}
