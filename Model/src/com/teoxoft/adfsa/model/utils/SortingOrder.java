package com.teoxoft.adfsa.model.utils;

public enum SortingOrder
{
    Ascending,
    Descending,
    Mixed
}