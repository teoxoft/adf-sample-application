package com.teoxoft.adfsa.model.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author: Gennadiy Pervukhin
 * Created: 16-10-2014
 */
public class ListWrapper<ListItemType>
{
    //
    //Constructors
    //
    public ListWrapper() {
        super();
    }
    //
    
    //
    //Variables
    //
    private List<ListItemType> list = new ArrayList<ListItemType>();
    //
    
    //
    //Properties
    //
    public List<ListItemType> getList() {
        return this.list;
    }
    //
    
    //
    //Public methods
    //
    public void addItem(ListItemType item)
    {
        this.list.add(item);
    }
    
    public void setList(Collection<ListItemType> newList)
    {
        this.list.clear();
        if (newList != null)
        {
            this.list.addAll(newList);
        }
    }
    
    public void setList(ListWrapper<ListItemType> newWrapper)
    {
        Collection<ListItemType> newList =
            (newWrapper != null ? newWrapper.getList() : null);
        this.setList(newList);
    }
    //
}
