package com.teoxoft.adfsa.model.utils.sql;

import com.teoxoft.adfsa.model.utils.ListWrapper;
import com.teoxoft.adfsa.model.utils.SortingParameter;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;


/**
 * @author: Gennadiy Pervukhin
 * Created: 16-10-2014
 */
public class SelectQueryBuilder extends SqlQueryBuilder
{
    //
    //Constructors
    //
    public SelectQueryBuilder(String selectedFieldsValue, String objectName, String objectSynonym)
    {
        super(objectName, objectSynonym);
        this.selectedFieldsValue = selectedFieldsValue;
    }
    
    public SelectQueryBuilder(String objectName, String objectSynonym)
    {
        this(null, objectName, objectSynonym);
    }
    //
    
    //
    //Variables
    //
    private String selectedFieldsValue;
    
    private ListWrapper<SortingParameter> sortingParameters = new ListWrapper<SortingParameter>();
    
    private int limit;
    private int offset;
    //
    
    //
    //Properties
    //
    public String getSelectedFieldsValue() {
        return selectedFieldsValue;
    }
    
    public ListWrapper<SortingParameter> getSortingParameters() {
        return sortingParameters;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
    public int getLimit() {
        return limit;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }
    public int getOffset() {
        return offset;
    }
    //
    
    //
    //Private methods
    //
    @Override
    protected String buildQueryString()
    {
        //check required data
        if (StringUtils.isBlank(this.objectName) || StringUtils.isBlank(this.objectSynonym))
        {
            return null;
        }
        
        //create main part of the query
        String queryString = "SELECT ";
        if (StringUtils.isNotBlank(this.selectedFieldsValue))
        {
            queryString += this.selectedFieldsValue;
        }
        else
        {
            queryString += this.objectSynonym;
        }
        queryString += " FROM " + this.objectName + " " + this.objectSynonym;
        
        //add 'WHERE' section to the query
        if (StringUtils.isNotBlank(this.condition))
        {
            queryString += " WHERE " + this.condition;
        }
        
        //add 'ORDER BY' section to the query
        if (this.sortingParameters.getList().size() > 0)
        {
            queryString += " ORDER BY ";
            //collect sorting expressions
            List<String> sortingExpressions = new ArrayList<String>();
            for (SortingParameter sortingParameter : this.sortingParameters.getList())
            {
                //generate a new sorting expression
                String sortingExpression = this.objectSynonym + "." + sortingParameter.getFieldName();
                if (!sortingParameter.isCaseSensitive())
                {
                    sortingExpression = "UPPER (" + sortingExpression + ")";
                }
                sortingExpression += " " + SqlUtils.getSqlOperator(sortingParameter.getSortingOrder());
                
                //add a new sorting expression into the list of all sorting expressions
                sortingExpressions.add(sortingExpression);
            }
            //join all sorting expressions via comma and add to a query string
            queryString += StringUtils.join(sortingExpressions, ", ");
        }
        
        return queryString;
    }
    //
    
    //
    //Public methods
    //
    public Query buildQuery(EntityManager entityManager)
    {
        //1) build a query for basic parameters
        Query query = super.buildQuery(entityManager);
        
        //2) try to set an offset
        if (this.offset > 0)
        {
            query.setFirstResult(this.offset);
        }
        
        //3) try to set a limit
        if (this.limit > 0)
        {
            query.setMaxResults(this.limit);
        }
        
        return query;
    }
    //
}
