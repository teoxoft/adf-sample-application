package com.teoxoft.adfsa.model.utils.sql;


import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;


/**
 * @author: Gennadiy Pervukhin
 * Created: 14-03-2014
 */
public abstract class SqlQueryBuilder
{
    //
    //Constructors
    //
    public SqlQueryBuilder(String objectName, String objectSynonym)
    {
        super();
        this.objectName = objectName;
        this.objectSynonym = objectSynonym;
    }
    //
    
    //
    //Variables
    //
    protected String objectName;
    protected String objectSynonym;
    
    protected String condition;
    protected Map<String, Object> parameters = new HashMap<String, Object>();
    //
    
    //
    //Properties
    //
    public String getObjectName() {
        return objectName;
    }

    public String getObjectSynonym() {
        return objectSynonym;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }
    public String getCondition() {
        return condition;
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }
    //
    
    //
    //Private methods
    //
    protected abstract String buildQueryString();
    //
    
    //
    //Public methods
    //
    public Query buildQuery(EntityManager entityManager)
    {
        //1) generate a query string
        String queryString = this.buildQueryString();
        
        //2) create a query object
        Query query = entityManager.createQuery(queryString);
        
        //3) set parameters
        for (String parameterName : this.parameters.keySet())
        {
            query.setParameter(parameterName, this.parameters.get(parameterName));
        }
        
        return query;
    }
    //
}