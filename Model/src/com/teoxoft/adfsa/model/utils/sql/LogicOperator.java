package com.teoxoft.adfsa.model.utils.sql;

public enum LogicOperator
{
    AND,
    OR
}