package com.teoxoft.adfsa.model.utils.sql;

import com.teoxoft.adfsa.model.filters.SearchPattern;
import com.teoxoft.adfsa.model.utils.SortingOrder;

import java.math.BigDecimal;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

/**
 * This class provides utilities for working with SQL
 *
 * @author: "Gennadiy Pervukhin"
 * Created: (4-12-2013)
 */
public class SqlUtils
{
    //
    //Constants
    //
    private static final String TEMPORARY_REPLACEMENT = UUID.randomUUID().toString();
    //
    
    //
    //Public methods
    //
    public static String getSqlOperator(SortingOrder sortingOrder)
    {
        if (sortingOrder == SortingOrder.Ascending)
        {
            return "ASC";
        }
        else if (sortingOrder == SortingOrder.Descending)
        {
            return "DESC";
        }
        else
        {
            return null;
        }
    }
    
    public static String getSqlOperator(LogicOperator logicOperator)
    {
        if (logicOperator == LogicOperator.AND)
        {
            return "AND";
        }
        else if (logicOperator == LogicOperator.OR)
        {
            return "OR";
        }
        else
        {
            return null;
        }
    }
    
    public static String convertSearchPatternForLikeOperator(String searchPattern)
    {
        if (StringUtils.isBlank(searchPattern)) {
            return searchPattern;
        }
        
        //set a map of requlars expresions and their replacements for special chars
        Map<String, String> regExpsAndReplacements = new HashMap<String, String>();
        regExpsAndReplacements.put("\\*", "%");
        
        //convert custom search pattern into SQL-pattern
        String convertedPattern = searchPattern;
        for (String reqularExpression : regExpsAndReplacements.keySet())
        {
            //1) replace all escaped sequences for the expression by temporary string
            convertedPattern = convertedPattern.replaceAll("\\\\" + reqularExpression,
                TEMPORARY_REPLACEMENT);
            //2) replace all occurences of the expression by SQL-analogue
            convertedPattern = convertedPattern.replaceAll(reqularExpression,
                regExpsAndReplacements.get(reqularExpression));
            //3) return back escaped sequences
            convertedPattern = convertedPattern.replaceAll(TEMPORARY_REPLACEMENT,
                "\\" + reqularExpression);
        }
        return convertedPattern;
    }
    
    public static String joinConditions(Collection<String> conditions, LogicOperator logicOperator)
    {
        //check passed data
        if (conditions == null) {
            return null;
        }
        
        //create a string separator
        String separator = " " + SqlUtils.getSqlOperator(logicOperator) + " ";
        //join conditions and return the result
        return StringUtils.join(conditions, separator);
    }
    
    public static void addCondition(SearchPattern searchPattern, String fieldName,
        String argumentName, List<String> conditions, SelectQueryBuilder queryBuilder)
    {
        if (searchPattern != null && searchPattern.isNotBlank())
        {
            //1) add a condition
            if (searchPattern.isCaseSensitive())
            {
                conditions.add(fieldName + " LIKE :" + argumentName);
            }
            else
            {
                conditions.add("UPPER(" + fieldName + ") LIKE UPPER(:" + argumentName + ")");
            }
            
            //2) add an argument
            String patternValue = searchPattern.getPattern();
            //if a pattern can contain special chars then we should convert it to SQL-pattern
            if (searchPattern.canContainSpecialChars())
            {
                patternValue = SqlUtils.convertSearchPatternForLikeOperator(patternValue);
            }
            queryBuilder.getParameters().put(argumentName, patternValue);
        }
    }
    //
}