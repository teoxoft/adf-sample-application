package com.teoxoft.adfsa.model.utils;

/**
 * @author: Gennadiy Pervukhin
 * Created: 31-03-2014
 */
public class SortingParameter
{
    //
    //Constructors
    //
    public SortingParameter(String name)
    {
        this(name, SortingOrder.Ascending, true);
    }
    
    public SortingParameter(String name, SortingOrder sortingOrder, boolean isCaseSensitive)
    {
        super();
        this.fieldName = name;
        this.sortingOrder = sortingOrder;
        this.isCaseSensitive = isCaseSensitive;
    }
    //
    
    //
    //Variables
    //
    private String fieldName;
    private SortingOrder sortingOrder;
    private boolean isCaseSensitive;
    //
    
    //
    //Properties
    //
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }
    public String getFieldName() {
        return fieldName;
    }

    public void setSortingOrder(SortingOrder sortingOrder) {
        this.sortingOrder = sortingOrder;
    }
    public SortingOrder getSortingOrder() {
        return sortingOrder;
    }

    public void setIsCaseSensitive(boolean isCaseSensitive) {
        this.isCaseSensitive = isCaseSensitive;
    }
    public boolean isCaseSensitive() {
        return isCaseSensitive;
    }
    //
}